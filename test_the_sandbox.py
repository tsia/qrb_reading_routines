#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  6 10:42:26 2024

@author: tsia
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
from w7x_overviewplot.query_programs import get_archive_byurl #, ns_to_utc
from w7x_overviewplot.query_programs.intervals import interval


use_pid = True

bolo_no = 6 # input number from 3 to 12 to check the respective cards in the respective slot positions

if use_pid:
    url_bolo_3 = ['/Test/raw/W7X/ControlStation.72602/BOLO_'+str(bolo_no)+'-1_DATASTREAM/0/ch0_sig/',
                  '/Test/raw/W7X/ControlStation.72602/BOLO_'+str(bolo_no)+'-1_DATASTREAM/1/ch0_pre/',
                  '/Test/raw/W7X/ControlStation.72602/BOLO_'+str(bolo_no)+'-1_DATASTREAM/2/ch0_cur/',
                  '/Test/raw/W7X/ControlStation.72602/BOLO_'+str(bolo_no)+'-1_DATASTREAM/3/ch0_vol/',
                  '/Test/raw/W7X/ControlStation.72602/BOLO_'+str(bolo_no)+'-1_DATASTREAM/4/ch1_sig/',
                  '/Test/raw/W7X/ControlStation.72602/BOLO_'+str(bolo_no)+'-1_DATASTREAM/5/ch1_pre/',
                  '/Test/raw/W7X/ControlStation.72602/BOLO_'+str(bolo_no)+'-1_DATASTREAM/6/ch1_cur/',
                  '/Test/raw/W7X/ControlStation.72602/BOLO_'+str(bolo_no)+'-1_DATASTREAM/7/ch1_vol/']
    
    pid = '20240917.037'
    
else:
    url_bolo_3 = ['/Sandbox/raw/W7X/ControlStation.72602/BOLO_'+str(bolo_no)+'-1_DATASTREAM/0/ch0_sig/',
                  '/Sandbox/raw/W7X/ControlStation.72602/BOLO_'+str(bolo_no)+'-1_DATASTREAM/1/ch0_pre/',
                  '/Sandbox/raw/W7X/ControlStation.72602/BOLO_'+str(bolo_no)+'-1_DATASTREAM/2/ch0_cal/',
                  '/Sandbox/raw/W7X/ControlStation.72602/BOLO_'+str(bolo_no)+'-1_DATASTREAM/3/ch0_exc/',
                  '/Sandbox/raw/W7X/ControlStation.72602/BOLO_'+str(bolo_no)+'-1_DATASTREAM/4/ch1_sig/',
                  '/Sandbox/raw/W7X/ControlStation.72602/BOLO_'+str(bolo_no)+'-1_DATASTREAM/5/ch1_pre/',
                  '/Sandbox/raw/W7X/ControlStation.72602/BOLO_'+str(bolo_no)+'-1_DATASTREAM/6/ch1_cal/',
                  '/Sandbox/raw/W7X/ControlStation.72602/BOLO_'+str(bolo_no)+'-1_DATASTREAM/7/ch1_exc/']
    
    from_ns = 1726586942481000000 # input beginning of timeseries in nanoseconds
    upto_ns = 1726586968633510000
    


plt.close('all')
fig, ax = plt.subplots(4,2)

for url in url_bolo_3:
    if use_pid:
        from_ns, upto_ns = interval(pid, target = 'ns', url = url)
    data_y, data_x, units = np.array(get_archive_byurl(url, [from_ns, upto_ns], pre=3, post=1), dtype=object)

    data_x = np.array(data_x)/1e9

    channel_no = url.split('ch')[1].split('_')[0]
    channel_name = url.split('ch')[1].split('_')[1]

    if channel_no == '0':
        j = 0
    else:
        j = 1
    if 'sig' in channel_name:
        i = 0
        color = 'tab:blue'
        label = 'sig'
    if 'pre' in channel_name:
        i = 1
        color = 'tab:orange'
        label = 'pre'
    if 'cur' in channel_name:
        i = 2
        color = 'tab:green'
        label = 'cal'
    if 'vol' in channel_name:
        i = 3
        color = 'tab:red'
        label = 'vol'
    ax[i,j].plot(data_x,data_y, color = color, label=label)
    ax[i,1].legend(loc = 'upper right')

ax[0,0].set_title('Channel 1')
ax[0,1].set_title('Channel 2')
ax[3,0].set_xlabel('time [s]')
ax[3,1].set_xlabel('time [s]')
ax[0,0].set_ylabel('signal [unit]')
ax[1,0].set_ylabel('signal [unit]')
ax[2,0].set_ylabel('signal [unit]')
ax[3,0].set_ylabel('signal [unit]')



